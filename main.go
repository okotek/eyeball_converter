package main

import (
	"encoding/gob"
	"flag"
	"net"
	"sync"
	"time"

	"gitlab.com/okotek/matbox"
	//"matbox"

	"gitlab.com/okotek/okoerr"
	"gitlab.com/okotek/okoframe"
	"gitlab.com/okotek/okonet"
)

func main() {

	sendoffTarget := flag.String("sendoffTarget", "localhost:8082", "Target to send the created frames to.")
	flag.Parse()

	matChan := make(chan matbox.MatBox)
	frameChan := make(chan okoframe.Frame)

	go matReciever(matChan, 0)
	go cvtToFrames(matChan, frameChan)
	go okonet.Sendoff(frameChan, *sendoffTarget)

	var wg sync.WaitGroup
	wg.Add(1)
	wg.Wait()
}

// Trying a little recursive-based resillience. Every recursive restart takes a linearly longer period of time to start
func matReciever(output chan matbox.MatBox, restartCount int) {

	if ln, lnErr := net.Listen("tcp", ":8645"); lnErr != nil {

		okoerr.ReportError("Fail to ln in matRec eyeball rec", "net", lnErr)
		time.Sleep(time.Duration(restartCount) * time.Millisecond)
		matReciever(output, restartCount+1)
		return
	} else {

		decMat := matbox.MatBox{}

		for {

			if con, conErr := ln.Accept(); conErr != nil {
				okoerr.ReportError("Fail to accept in matReciever", "net", conErr)
				time.Sleep(time.Duration(restartCount) * time.Millisecond)
				matReciever(output, restartCount+1)
				return
			} else {
				if decErr := gob.NewDecoder(con).Decode(&decMat); decErr != nil {
					okoerr.ReportError("failed to decode in matReciever", "net", decErr)
					output <- decMat
				}

				output <- decMat

			}

		}
	}
}

func cvtToFrames(input chan matbox.MatBox, output chan okoframe.Frame) {

	for tmpBox := range input {

		go func(targMat matbox.MatBox) {
			output <- targMat.ConvertToFrame()
		}(tmpBox)

	}
}
