module eyeball_converter

go 1.22.0

require gitlab.com/okotek/okoframe v0.0.1

require gitlab.com/okotek/okoerr v1.1.1

require gitlab.com/okotek/matbox v0.0.1

require (
	gitlab.com/okotek/okonet v1.1.1
	gocv.io/x/gocv v0.35.0
)

require gitlab.com/ashinnv/oddstring v0.1.1 // indirect

replace gitlab.com/okotek/okoframe => ../okoframe/

replace gitlab.com/okotek/okoerr => ../okoerr

replace gitlab.com/okotek/okonet => ../okonet

replace gitlab.com/okotek/matbox => ../matbox/
